# GodotUnityAds
Godot UnityAds module for Android and iOS.

[![Platform](https://img.shields.io/badge/Platform-Android-green.svg)](#)
[![Platform](https://img.shields.io/badge/Platform-iOS-green.svg)](#)
[![GodotEngine](https://img.shields.io/badge/Godot_Engine-3.1.X-blue.svg)](https://github.com/godotengine/godot)
[![LICENCE](https://img.shields.io/badge/License-Apache_V2-blue.svg)](https://www.apache.org/licenses/LICENSE-2.0)

# Compiling export template

In order to compile a new export template which includes this module, you need
to get the `Godot` source code, and then put `GodotUnityAds` in the `modules`
directory:

`${GODOT_ROOT}` refers to the path where you have `Godot` source code.

```
cd ${GODOT_ROOT}/modules/
git clone https://gitlab.com/Openhood/Godot/GodotUnityAds godot_unity_ads
```

Then you will have to download and move the Unity ads SDKs for Android and iOS
into the correct directories as described below:

## Android:

Download the [Unity-ads SDK for Android](https://github.com/Unity-Technologies/unity-ads-android/releases/download/3.3.0/unity-ads.aar.zip) to `${GODOT_ROOT}/modules/godot_unity_ads/android/libs/` (uncompress it if it's a .zip file)

Go the root of the `Godot` source code, and compile:

```
cd ${GODOT_ROOT}
# You can replace 2 by the number of cpu/cores you have to speed things up
scons -j 2 platform=android target=release android_arch=armv7
scons -j 2 platform=android target=release android_arch=arm64v8
scons -j 2 platform=android target=release_debug android_arch=armv7
scons -j 2 platform=android target=release_debug android_arch=arm64v8
cd platform/android/java/
./gradlew build
```

When it's done, you will have a new Android templates at:

- `${GODOT_ROOT}/bin/android_debug.apk` : for the debug template
- `${GODOT_ROOT}/bin/android_release.apk` : for the release template

You can move them around if you want, or keep them here and configure them in
Godot editor in the menu: `Project > Export` then in your `Android` preset
select the debug one for `Custom Package > Debug` and the release one for
`Custom Package > Release`.

## iOS:

Download the [Unity-ads SDK for iOS](https://github.com/Unity-Technologies/unity-ads-ios/releases/download/3.3.0/UnityAds.framework.zip) to `${GODOT_ROOT}/modules/godot_unity_ads/ios/libs/` (uncompress it if it's a .zip file)

Go the root of the `Godot` source code, and compile:

```
cd ${GODOT_ROOT}
# You can replace 2 by the number of cpu/cores you have to speed things up
scons -j 2 platform=iphone target=release arch=arm
scons -j 2 platform=iphone target=release arch=arm64
scons -j 2 platform=iphone target=debug arch=arm
scons -j 2 platform=iphone target=debug arch=arm64
```

When it's done, you will have to combine the static godot libs into `universal`
(or `fat`) ones:

```
cd ${GODOT_ROOT}
lipo -create bin/libgodot.iphone.opt.arm.a bin/libgodot.iphone.opt.arm64.a -output bin/libgodot.iphone.release.fat.a
lipo -create bin/libgodot.iphone.debug.arm.a bin/libgodot.iphone.debug.arm64.a -output bin/libgodot.iphone.debug.fat.a
```

Then copy the new static library to the `ios_xcode` template:

```
cd ${GODOT_ROOT}
cp bin/libgodot.iphone.release.fat.a bin/libgodot.iphone.debug.fat.a misc/dist/ios_xcode/
```

Finally create a new ios template:

```
cd ${GODOT_ROOT}/misc/dist/
# this is needed because the exporter is expecting the directory from the zip
# to be named `iphone`
cp -R ios_xcode iphone
zip -x \*.DS_Store -r iphone.zip iphone
rm -rf iphone
```

You can move it around if you want, or keep it here and configure it in
Godot editor in the menu: `Project > Export` then in your `iOS` preset
select it for both `Custom Package > Debug` and `Custom Package > Release`.

If you want it to become the default you can copy them to the Godot's template
directory, based on your OS:

```
Windows: %APPDATA%\Godot\templates\<version>\
Linux: $HOME/.local/share/godot/templates/<version>/
macOS: $HOME/Library/Application Support/Godot/templates/<version>/
```

Example for macOS:

```
cd ${GODOT_ROOT}
# Android
cp bin/android_debug.apk $HOME/Library/Application\ Support/Godot/templates/3.1.1.stable/android_debug.apk
cp bin/android_release.apk $HOME/Library/Application\ Support/Godot/templates/3.1.1.stable/android_release.apk
# iPhone
cp misc/dist/iphone.zip $HOME/Library/Application\ Support/Godot/templates/3.1.1.stable/
```

# Using the new template in your `Godot` project:

## Edit project.godot and add:

```
[android]

modules="org/godotengine/godot/godot_unity_ads"
```

## Initialize the module

Preferably in a script loaded as soon as the game starts.

```gdscript
var GodotUnityAds = Engine.get_singleton("GodotUnityAds")

# Initialize GodotUnityAds, default to test mode
GodotUnityAds.init("Your game id", get_instance_id())

# Or by specifying the test mode manually
GodotUnityAds.initWithTestMode("Your game id", get_instance_id(), false)
```

## Show an Ad:

```gdscript
var GodotUnityAds = Engine.get_singleton("GodotUnityAds")
GodotUnityAds.show("myPlacementId") # Example value for default placements in UnityAds: "video" or "rewardedVideo"
```

# API

```gdscript
"""
returns bool - Returns true if Debug Mode is enabled.
"""
GodotUnityAds.getDebugMode()
```

```gdscript
"""
parameters:
  debugMode: bool - Logs are verbose when set to true, and minimal when false.
"""
GodotUnityAds.setDebugMode(debug_mode)
```

```gdscript
"""
Returns the state of the specified ad placement ID. If a placement is not specified, the default ad placement will be used instead.
returns string -
  "READY" -	A campaign is available and ready to be shown.
  "NOT_AVAILABLE" -	Either the SDK is not yet initialized, or the specified placement ID is invalid. For a placement ID to be valid, it must be listed among the placements for the Android platform of your project in the Unity Ads dashboard. Note: Each platform has its own list of placements. When using the same custom placement ID for both Android and iOS, the placement must be added to both platforms.
  "DISABLED" -	The specified placement is currently disabled for the Android platform of your project. Placement settings can be updated from the Unity Ads dashboard.
  "WAITING" -	The specified placement is in the process of becoming ready.
  "NO_FILL" -	There are no campaigns currently available.
"""
GodotUnityAds.getPlacementState(placement_id)
```

```gdscript
"""
returns string - the Unity Ads SDK version
"""
GodotUnityAds.getVersion()
```

```gdscript
"""
Initializes the Unity Ads SDK using the specified Game ID.

parameters:
  game_id: string - Your unity Game ID
  test_mode: bool - default: false - Enables Test Mode when set to true. Test Mode defaults to being disabled when a value is not specified. While Test Mode is enabled, only test ads will be shown. Test ads do not generate any stats or revenue.
"""
GodotUnityAds.initialize(game_id, test_mode)
```

```gdscript
"""
return bool - true if the Unity Ads SDK is initialized
"""
GodotUnityAds.isInitialized()
```

```gdscript
"""
parameters:
  placement_id: string - The ad placement identifier. This can be found listed under the Android platform of your project in the Unity Ads dashboard.

return bool - true if the specified ad placement is ready to show an ad campaign. If a placement is not specified, the default ad placement will be used instead.
"""
GodotUnityAds.isReady(placement_id)
```

```gdscript
"""
return bool - true if Unity Ads is supported by the current device. Call this method before attempting to initialize Unity Ads.
"""
GodotUnityAds.isSupported()
```

```gdscript
"""
Shows an ad campaign using the specified ad placement. If no placement is specified, the default placement is used instead.

After the ad is shown, the method on_UnityAds_finish will be called when the ad placement is closed. This callback method can be used to reward players for opting-in to watch video ads.

parameters:
  placement_id: string - The ad placement identifier. This can be found listed under the Android platform of your project in the Unity Ads dashboard.

"""
GodotUnityAds.show(placement_id)
```

# Callbacks

The UnityAds sdk send callbacks at critical moments, here's the list of callbacks you can implement in the script you registered as delegate when calling `GodotUnityAds.init("your game id", get_instance_id())`:

```gdscript
"""
In addition to error logs, this method is called when an error occurs with Unity Ads. This method can be used to assist in debugging efforts, as well as for collect statistics on various failure scenarios.

parameters:

  error: String - The Unity Ads error that occurred.
    "NOT_INITIALIZED"	An attempt to show an ad was made before Unity Ads was initialized.
    "INITIALIZE_FAILED"	Some condition prevented Unity Ads from being initialized.
    "INVALID_ARGUMENT"	An invalid value was passed to the Unity Ads API.
    "VIDEO_PLAYER_ERROR"	An error was encountered during video playback.
    "INIT_SANITY_CHECK_FAIL"	The device environment sanity check failed. Unity Ads will not function.
    "AD_BLOCKER_DETECTED"	An ad blocker was detected running on device. Unity Ads will not function.
    "FILE_IO_ERROR"	An error occurred while attempting to write a file to device storage.
    "DEVICE_ID_ERROR"	The device ID was not found or was invalid. Unity Ads will not function.
    "SHOW_ERROR"	An attempt to show an ad failed before the video started.
    "INTERNAL_ERROR"	Some other internal error occurred.
  message: String - A description of the error that occurred.
"""

func _on_GodotUnityAds_error(error: String, message: String) -> void:
  pass
```

```gdscript
"""
This method is called after the ad placement is closed. For each call to UnityAds.show, there will be a call to IUnityAdsListener.onUnityAdsFinish, including all failure scenarios.

parameters:

  placement_id: String - The ad placement identifier. This can be found listed under the Android platform of your project in the Unity Ads dashboard.
  result: String - The state in which the ad was finished.
    "ERROR"	- An error occurred that prevented the ad from being shown.
    "COMPLETED" -	The ad was shown from beginning to end without error and without having been skipped.
    "SKIPPED" -	The ad was skipped before reaching the end.

"""
func _on_GodotUnityAds_finish(placement_id: String, result: String) -> void:
  pass
```

```gdscript
"""
This method is called when the specified ad placement becomes ready to show an ad campaign.

parameters:

  placementId: String - The ad placement identifier. This can be found listed under the Android platform of your project in the Unity Ads dashboard.
"""
func _on_GodotUnityAds_ready(placement_id: String) -> void:
  pass

```gdscript
"""
This method is called at the start of video playback for the ad campaign being shown.

parameters:

  placementId: String - The ad placement identifier. This can be found listed under the Android platform of your project in the Unity Ads dashboard.
"""
func _on_GodotUnityAds_start(placement_id: String) -> void:
  pass
```

# Log adb

```bash
adb -d logcat godot:V GoogleService:V DEBUG:V AndroidRuntime:V ValidateServiceOp:V *:S
```
