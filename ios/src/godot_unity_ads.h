#ifndef GODOT_UNITY_ADS_H
#define GODOT_UNITY_ADS_H

#include <core/reference.h>

#ifdef __OBJC__
@class GodotUnityAdsDelegate;
typedef GodotUnityAdsDelegate *delegatePtr;
@class ViewController;
typedef ViewController *viewControllerPtr;
#else
typedef void *delegatePtr;
typedef void *viewControllerPtr;
#endif

class GodotUnityAds : public Reference {

  GDCLASS(GodotUnityAds, Reference);

  String gameId;
  int instanceId;
  delegatePtr adsDelegate;
  viewControllerPtr rootController;

protected:

  static void _bind_methods();

public:

  GodotUnityAds();
  ~GodotUnityAds();

  void init(const String &p_gameId, int p_instanceId);
  void initWithTestMode(const String &p_gameId, int p_instanceId, bool testMode);
  bool getDebugMode();
  void setDebugMode(bool debugMode);
  String getPlacementState(const String &placementId);
  bool isReady(const String &placementId);
  String getVersion();
  bool isInitialized();
  bool isSupported();
  void show(const String &placementId);
};

#endif
