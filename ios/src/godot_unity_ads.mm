#include "godot_unity_ads.h"

#import "app_delegate.h"
#import "godot_unity_ads_delegate.h"

#ifdef DEBUG_ENABLED
  #define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#else
  #define DLog(fmt, ...) /* do when in release */
#endif

GodotUnityAds::GodotUnityAds() {
}

GodotUnityAds::~GodotUnityAds() {
}

void GodotUnityAds::init(const String &p_gameId, int p_instanceId) {
  initWithTestMode(p_gameId, p_instanceId, false);
}

void GodotUnityAds::initWithTestMode(const String &p_gameId, int p_instanceId, bool testMode) {
  gameId = p_gameId;
  instanceId = p_instanceId;
  rootController = [AppDelegate getViewController];
  adsDelegate = [GodotUnityAdsDelegate alloc];
  [adsDelegate initWithInstanceId:instanceId];

  if ([UnityAds isSupported]) {
    NSString *gameIdStr =
      [NSString stringWithCString:p_gameId.utf8().get_data()
                         encoding:NSUTF8StringEncoding];
    [UnityAds initialize:gameIdStr delegate:adsDelegate testMode:testMode];

    DLog(@"GodotUnityAds: initialized");
  } else {
    DLog(@"GodotUnityAds: Unity Ads is not supported by the current device");
  }
}

bool GodotUnityAds::getDebugMode() {
  return [UnityAds getDebugMode];
}

void GodotUnityAds::setDebugMode(bool debugMode) {
  [UnityAds setDebugMode:debugMode];
}

String GodotUnityAds::getPlacementState(const String &placementId) {
  NSString *placementIdStr =
    [NSString stringWithCString:placementId.utf8().get_data()
                       encoding:NSUTF8StringEncoding];
  UnityAdsPlacementState state = [UnityAds getPlacementState:placementIdStr];
  NSString *stateStr = @"UNKNOWN";
  switch (state) {
    case kUnityAdsPlacementStateReady:
      stateStr = @"READY";
      break;
    case kUnityAdsPlacementStateNotAvailable:
      stateStr = @"NOT_AVAILABLE";
      break;
    case kUnityAdsPlacementStateDisabled:
      stateStr = @"DISABLED";
      break;
    case kUnityAdsPlacementStateWaiting:
      stateStr = @"WAITING";
      break;
    case kUnityAdsPlacementStateNoFill:
      stateStr = @"NO_FILL";
      break;
    default:
      break;
  }
  return [stateStr UTF8String];
}

bool GodotUnityAds::isReady(const String &placementId){
  NSString *placementIdStr =
    [NSString stringWithCString:placementId.utf8().get_data()
                       encoding:NSUTF8StringEncoding];
  return [UnityAds isReady:placementIdStr];
}

String GodotUnityAds::getVersion(){
  return [[UnityAds getVersion] UTF8String];
}

bool GodotUnityAds::isInitialized(){
  return [UnityAds isInitialized];
}

bool GodotUnityAds::isSupported(){
  return [UnityAds isSupported];
}

void GodotUnityAds::show(const String &placementId){
  NSString *placementIdStr =
    [NSString stringWithCString:placementId.utf8().get_data()
                       encoding:NSUTF8StringEncoding];

  if ([UnityAds isReady:placementIdStr]) {
    [UnityAds show:rootController placementId:placementIdStr];
  } else {
    DLog(@"GodotUnityAds: trying to show %@, but ad is not ready.", placementIdStr);
  }
}

// ~~~ Godot specific methods ~~~
void GodotUnityAds::_bind_methods() {
  ClassDB::bind_method("init", &GodotUnityAds::init);
	ClassDB::bind_method("initWithTestMode", &GodotUnityAds::initWithTestMode);
	ClassDB::bind_method("getDebugMode", &GodotUnityAds::getDebugMode);
	ClassDB::bind_method("setDebugMode", &GodotUnityAds::setDebugMode);
	ClassDB::bind_method("getPlacementState", &GodotUnityAds::getPlacementState);
	ClassDB::bind_method("isReady", &GodotUnityAds::isReady);
  ClassDB::bind_method("getVersion", &GodotUnityAds::getVersion);
	ClassDB::bind_method("isInitialized", &GodotUnityAds::isInitialized);
  ClassDB::bind_method("isSupported", &GodotUnityAds::isSupported);
	ClassDB::bind_method("show", &GodotUnityAds::show);
}
