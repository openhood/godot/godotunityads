#import "godot_unity_ads_delegate.h"
#include <core/object.h>

#ifdef DEBUG_ENABLED
  #define DLog(fmt,...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, __VA_ARGS__)
#else
  #define DLog(fmt,...) /* do when in release */
#endif

@implementation GodotUnityAdsDelegate

- (void) initWithInstanceId:(int)p_instanceId {
  initialized = true;
  instanceId = p_instanceId;
  DLog(@"GodotUnityAdsDelegate initialized %i", instanceId);
}

// ~~~ UnityAdsDelegate ~~~
- (void) unityAdsReady:(NSString *)placementId {
  DLog(@"GodotUnityAds unityAdsReady %@", placementId);
  Object *obj = ObjectDB::get_instance(instanceId);
  obj->call_deferred("_on_GodotUnityAds_ready", [placementId UTF8String]);
}

- (void) unityAdsDidError:(UnityAdsError)error withMessage:(NSString *)message {
  NSString *errorString = @"UNKNOWN";
  switch (error) {
    case kUnityAdsErrorNotInitialized:
      errorString = @"NOT_INITIALIZED";
      break;
    case kUnityAdsErrorInitializedFailed:
      errorString = @"INITIALIZE_FAILED";
      break;
    case kUnityAdsErrorInvalidArgument:
      errorString = @"INVALID_ARGUMENT";
      break;
    case kUnityAdsErrorVideoPlayerError:
      errorString = @"VIDEO_PLAYER_ERROR";
      break;
    case kUnityAdsErrorInitSanityCheckFail:
      errorString = @"INIT_SANITY_CHECK_FAIL";
      break;
    case kUnityAdsErrorAdBlockerDetected:
      errorString = @"AD_BLOCKER_DETECTED";
      break;
    case kUnityAdsErrorFileIoError:
      errorString = @"FILE_IO_ERROR";
      break;
    case kUnityAdsErrorDeviceIdError:
      errorString = @"DEVICE_ID_ERROR";
      break;
    case kUnityAdsErrorShowError:
      errorString = @"SHOW_ERROR";
      break;
    case kUnityAdsErrorInternalError:
      errorString = @"INTERNAL_ERROR";
      break;
    default:
      break;
  }
  DLog(@"GodotUnityAds unityAdsDidError, %ld = %@ - %@", (long)error, errorString, message);
  Object *obj = ObjectDB::get_instance(instanceId);
  obj->call_deferred("_on_GodotUnityAds_error", [errorString UTF8String]);
}

- (void) unityAdsDidStart:(NSString *)placementId {
  DLog(@"GodotUnityAds unityAdsDidStart %@", placementId);
  Object *obj = ObjectDB::get_instance(instanceId);
  obj->call_deferred("_on_GodotUnityAds_start", [placementId UTF8String]);
}

- (void) unityAdsDidFinish:(NSString *)placementId withFinishState:(UnityAdsFinishState)state {
  NSString *stateString = @"UNKNOWN";
  switch (state) {
    case kUnityAdsFinishStateError:
      stateString = @"ERROR";
      break;
    case kUnityAdsFinishStateSkipped:
      stateString = @"SKIPPED";
      break;
    case kUnityAdsFinishStateCompleted:
      stateString = @"COMPLETED";
      break;
    default:
      break;
  }
  DLog(@"GodotUnityAds unityAdsDidFinish %@ - %ld (%@)", placementId, (long)state, stateString);
  Object *obj = ObjectDB::get_instance(instanceId);
  obj->call_deferred("_on_GodotUnityAds_finish", [placementId UTF8String], [stateString UTF8String]);
}

@end
