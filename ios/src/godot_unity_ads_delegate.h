#import <UnityAds/UnityAds.h>

@interface GodotUnityAdsDelegate: NSObject <UnityAdsDelegate> {
  bool initialized;
  int instanceId;
}

- (void)initWithInstanceId:(int)p_instanceId;

@end
