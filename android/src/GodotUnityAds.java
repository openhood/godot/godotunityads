/**
 * Copyright 2019 Openhood sarl. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.godotengine.godot;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;

import com.unity3d.ads.IUnityAdsListener;
import com.unity3d.ads.UnityAds;

import com.godot.game.BuildConfig;
import com.godot.game.R;

import org.godotengine.godot.Godot;

public class GodotUnityAds extends Godot.SingletonBase {
	private Activity activity = null;
	private String gameId = null;
	private int instanceId = -1;

	static public Godot.SingletonBase initialize(Activity activity)
	{
		return new GodotUnityAds(activity);
	}

	public GodotUnityAds(Activity p_activity) {
		registerClass("GodotUnityAds", new String[] {
			// initialization
			"init", "initWithTestMode",
			// debug
			"getDebugMode", "setDebugMode",
			// placement infos
			"getPlacementState", "isReady",
			// UnityAds sdk infos
			"getVersion", "isInitialized", "isSupported",
			// ads display
			"show"
		});
		activity = p_activity;
	}

	public void init(final String p_gameId, final int p_instanceId) {
		this.initWithTestMode(p_gameId, p_instanceId, false);
	}

	public void initWithTestMode(final String p_gameId, final int p_instanceId, final boolean testMode) {
		this.gameId = p_gameId;
		this.instanceId = p_instanceId;

		if (UnityAds.isSupported()) {
			activity.runOnUiThread(new Runnable() {
				@Override public void run()
				{
						UnityAds.setListener(listener);
						if (testMode || BuildConfig.DEBUG) {
							UnityAds.setDebugMode(true);
						}
						UnityAds.initialize(activity, gameId, listener, testMode);

						Log.d("godot", "GodotUnityAds: initialized");
				}
			});
		} else {
			Log.d("godot", "GodotUnityAds: Unity Ads is not supported by the current device");
		}
	}

	public boolean getDebugMode() {
		return UnityAds.getDebugMode();
	}

	public void setDebugMode(boolean debugMode) {
		UnityAds.setDebugMode(debugMode);
	}

	public String getPlacementState(String placementId) {
		return UnityAds.getPlacementState(placementId).toString();
	}

	public boolean isReady(String placementId) {
		return UnityAds.isReady(placementId);
	}

	public String getVersion() {
		return UnityAds.getVersion();
	}

	public boolean isInitialized() {
		return UnityAds.isInitialized();
	}

	public boolean isSupported() {
		return UnityAds.isSupported();
	}

	public void show(String placementId) {
		if (this.isReady(placementId)) {
			UnityAds.show(activity, placementId);
		} else {
			Log.d("godot", "GodotUnityAds: trying to show " + placementId + ", but ad is not ready.");
		}
	}

	private IUnityAdsListener listener = new IUnityAdsListener() {
		@Override
		public void onUnityAdsError(UnityAds.UnityAdsError error, String message) {
			Log.d("godot", "GodotUnityAds: onUnityAdsError: " + error + " - " + message);
			GodotLib.calldeferred(instanceId, "_on_GodotUnityAds_error", new Object[] { error.toString(), message });
		}

		@Override
		public void onUnityAdsFinish(String placementId, UnityAds.FinishState result) {
			Log.d("godot", "GodotUnityAds: onUnityAdsFinish: " + placementId + " - " + result);
			GodotLib.calldeferred(instanceId, "_on_GodotUnityAds_finish", new Object[] { placementId, result.toString() });
		}

		@Override
		public void onUnityAdsReady(String placementId) {
			Log.d("godot", "GodotUnityAds: onUnityAdsReady: " + placementId);
			GodotLib.calldeferred(instanceId, "_on_GodotUnityAds_ready", new Object[] { placementId });
		}

		@Override
		public void onUnityAdsStart(String placementId) {
			Log.d("godot", "GodotUnityAds: onUnityAdsStart: " + placementId);
			GodotLib.calldeferred(instanceId, "_on_GodotUnityAds_start", new Object[] { placementId });
		}
	};
}
