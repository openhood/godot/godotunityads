"""
# Copyright 2019 Openhood sarl. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#	http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""

import os

RED   = "\033[1;31m"
GREEN = "\033[0;32m"
RESET = "\033[0;0m"

def can_build(env, platform):
  if platform == "android" or platform == "iphone":
    print("GodotUnityAds: " + GREEN + "Enabled" + RESET)
    return True
  else:
    print("GodotUnityAds: " + RED + "Disabled" + RESET)
    return False

def configure(env):
  if env["platform"] == "android":
    cur_dir = os.path.dirname(os.path.abspath(__file__)).replace(os.path.sep, '/')
    libpath = os.path.join(cur_dir, "android", "libs").replace(os.path.sep, '/')
    env.android_add_dependency(
      "implementation ('com.google.android.gms:play-services-ads:16.0.0') {\
        exclude group: 'com.android.support'\
        exclude module: 'support-v4'}")
    env.android_add_java_dir("android/src")
    env.android_add_to_manifest("android/AndroidManifestChunk.xml")
    env.android_add_to_permissions("android/AndroidPermissionsChunk.xml")
    env.android_add_flat_dir(libpath)
    env.android_add_dependency("implementation(name:'unity-ads', ext:'aar')")
    env.disable_module()

  if env["platform"] == "iphone":
    env.Append(FRAMEWORKPATH=['#modules/godot_unity_ads/ios/libs'])
    env.Append(CPPPATH=['#core'])
    env.Append(LINKFLAGS=[
      '-ObjC',
      '-framework','AdSupport',
      '-framework','CoreTelephony',
      '-framework','EventKit',
      '-framework','EventKitUI',
      '-framework','MessageUI',
      '-framework','StoreKit',
      '-framework','SafariServices',
      '-framework','CoreBluetooth',
      '-framework','AssetsLibrary',
      '-framework','CoreData',
      '-framework','CoreLocation',
      '-framework','CoreText',
      '-framework','ImageIO',
      '-framework','GLKit',
      '-framework','CoreVideo',
      '-framework','CFNetwork',
      '-framework','MobileCoreServices',
      '-framework','UnityAds'
    ])
    # print(env.Dump())
