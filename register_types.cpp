#include <core/engine.h>

#include "register_types.h"
#include "ios/src/godot_unity_ads.h"

void register_godot_unity_ads_types() {
  Engine::get_singleton()->add_singleton(
    Engine::Singleton("GodotUnityAds", memnew(GodotUnityAds))
  );
}

void unregister_godot_unity_ads_types() {
}
